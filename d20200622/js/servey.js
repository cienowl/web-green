function formConfirm(form){

    var flag = 0;
    var name = form.name.value;
    var phone_number = form.phone_number.value;
    var email = form.email.value;

    var inputSelector = document.querySelectorAll("#personal_info > input");
    var labelSelect = document.querySelectorAll("#personal_info > label");

    for(var i = 0; i < inputSelector.length; i++){
        if(inputSelector[i].value == ""){
            alert(labelSelect[i].innerText + "항목을 입력해주세요.");
            inputSelector[i].focus();
            flag = 1;
            break;
        }
    }

    if(form.known.value != ""){
        var known_method = form.known.value;
    } else {
        alert("알게된 경로를 선택해주세요.");
        flag = 1;
    }    

    var checkbox_count = document.getElementsByName("sports").length;
    var favorite_sports='';
    var checked = 0;
    for (var i = 0; i < checkbox_count; i++) {
        if (document.getElementsByName("sports")[i].checked == true) {
            favorite_sports += document.getElementsByName("sports")[i].value + ", ";
            checked++;
        }
    }

    if (checked == 0){
        alert("좋아하는 스포츠를 선택해주세요.");
        flag = 1;
    }

    var contact_method = form.method.value;
    
    if(form.personalinfo.checked == false){
        alert("개인정보 활용에 동의해주세요.");
        flag = 1;
    } else {
        var personal_info_agreement = true;
    }

    if(flag == 0 && personal_info_agreement == true){
        alert(name + "\n" + phone_number + "\n" + email + "\n" + known_method + "\n" + favorite_sports + "\n" + contact_method + "\n" + personal_info_agreement);
        location.reload(true);
    }
 
}
