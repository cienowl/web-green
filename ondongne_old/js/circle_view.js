$(document).ready(function(){
	$("#signup").click(function() {
		var emptyfields = document.querySelectorAll(".form-control");
		console.log(emptyfields.length);
		if(emptyfields[0]==""){
			$(".form-group has-warning has-feedback").attr("class","form-group has-error has-feedback");
			$(this).attr('id',"inputError2");
			$(this).attr('aria-describedby',"inputError2Status");
		}
		if (emptyfields.length> 0) {
			$(this).stop()
				.animate({ left: "-10px" }, 100).animate({ left: "10px" }, 100)
				.animate({ left: "-10px" }, 100).animate({ left: "10px" }, 100)
				.animate({ left: "0px" }, 100)
				.addClass("required");
		}
	});
});