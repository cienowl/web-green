$(document).ready(function(){
    $('#gnb_01').hover(
        function(){
            $(this).addClass('hover_header_main_nav');                    
            $(this).children('img').attr('src', 'img/gnb_01_on.png');
            $('#header_main_nav span').attr('style', 'opacity: 1; color: #6f6f6f');
            $(this).children('span').attr('style', 'color: white');
            $('#gnb_layer').attr('style', 'opacity: 1');
            $('header hr').attr('style', 'background-color: #4f4c4b');
        },
        function(){
            $(this).removeClass('hover_header_main_nav');                    
            $(this).children('img').attr('src', 'img/gnb_01.png');
            $(this).children('span').attr('style', 'opacity: 0');
            $(this).children('span').attr('style', 'color: #6f6f6f');
            
           
        }
    );
    $('#gnb_02').hover(
        function(){
            $(this).addClass('hover_header_main_nav');                    
            $(this).children('img').attr('src', 'img/gnb_02_on.png');
            $('#header_main_nav span').attr('style', 'opacity: 1; color: #6f6f6f');
            $(this).children('span').attr('style', 'color: white');
            $('#gnb_layer').attr('style', 'opacity: 1');
            $('header hr').attr('style', 'background-color: #4f4c4b');
        },
        function(){
            $(this).removeClass('hover_header_main_nav');                    
            $(this).children('img').attr('src', 'img/gnb_02.png');
            $(this).children('span').attr('style', 'opacity: 0');
            $(this).children('span').attr('style', 'color: #6f6f6f');
            
           
        }
    );
    $('#gnb_03').hover(
        function(){
            $(this).addClass('hover_header_main_nav');                    
            $(this).children('img').attr('src', 'img/gnb_03_on.png');
            $('#header_main_nav span').attr('style', 'opacity: 1; color: #6f6f6f');
            $(this).children('span').attr('style', 'color: white');
            $('#gnb_layer').attr('style', 'opacity: 1');
            $('header hr').attr('style', 'background-color: #4f4c4b');
        },
        function(){
            $(this).removeClass('hover_header_main_nav');                    
            $(this).children('img').attr('src', 'img/gnb_03.png');
            $(this).children('span').attr('style', 'opacity: 0');
            $(this).children('span').attr('style', 'color: #6f6f6f');
           
        }
    );
    $('#gnb_04').hover(
        function(){
            $(this).addClass('hover_header_main_nav');                    
            $(this).children('img').attr('src', 'img/gnb_04_on.png');
            $('#header_main_nav span').attr('style', 'opacity: 1; color: #6f6f6f');
            $(this).children('span').attr('style', 'color: white');
            $('#gnb_layer').attr('style', 'opacity: 1');
            $('header hr').attr('style', 'background-color: #4f4c4b');
        },
        function(){
            $(this).removeClass('hover_header_main_nav');                    
            $(this).children('img').attr('src', 'img/gnb_04.png');
            $(this).children('span').attr('style', 'opacity: 0');
            $(this).children('span').attr('style', 'color: #6f6f6f');
            
        }
    );
    $('#gnb_05').hover(
        function(){
            $(this).addClass('hover_header_main_nav');                    
            $(this).children('img').attr('src', 'img/gnb_05_on.png');
            $('#header_main_nav span').attr('style', 'opacity: 1; color: #6f6f6f');
            $(this).children('span').attr('style', 'color: white');
            $('#gnb_layer').attr('style', 'opacity: 1');
            $('header hr').attr('style', 'background-color: #4f4c4b');
        },
        function(){
            $(this).removeClass('hover_header_main_nav');                    
            $(this).children('img').attr('src', 'img/gnb_05.png');
            $(this).children('span').attr('style', 'opacity: 0');   
            $(this).children('span').attr('style', 'color: #6f6f6f');
        }
    );
    $('#gnb_06').hover(
        function(){
            $(this).addClass('hover_header_main_nav');                    
            $(this).children('img').attr('src', 'img/gnb_06_on.png');
            $('#header_main_nav span').attr('style', 'opacity: 1; color: #6f6f6f');
            $(this).children('span').attr('style', 'color: white');
            $('#gnb_layer').attr('style', 'opacity: 1');
            $('header hr').attr('style', 'background-color: #4f4c4b');
        },
        function(){
            $(this).removeClass('hover_header_main_nav');                    
            $(this).children('img').attr('src', 'img/gnb_06.png');
            $(this).children('span').attr('style', 'opacity: 0');    
            $(this).children('span').attr('style', 'color: #6f6f6f');        
        }
    );

    $('#gnb_layer').hover(
        function(){

        },
        function(){
            $('#gnb_layer').attr('style', 'opacity: 0');
            $('header hr').attr('style', 'background-color: #ba000c');
            $('#header_main_nav span').attr('style', 'opacity: 0');
        }
    );

    $('.bxslider').bxSlider();
    
});