$(document).ready(function(){

    $('#top_nav li').hover(
        function(){
            $(this).removeClass('theme-white');
            $(this).addClass('theme-black');                    
            $(this).children('a').attr('style', 'color: white;');
        },
        function(){
            $(this).removeClass('theme-black');                    
            $(this).addClass('theme-white');
            $(this).children('a').attr('style', 'color: black;');
        }
    );

    $('#main_nav li').hover(
        function(){
            $(this).removeClass('theme-white');
            $(this).addClass('theme-black');
            $(this).children('a').attr('style', 'color: white;');                    
        },
        function(){
            $(this).removeClass('theme-black');                    
            $(this).addClass('theme-white');
            $(this).children('a').attr('style', 'color: black;');                    
        }
    );

    $('.first_row td').hover(
        function(){
            $(this).removeClass('theme-white');
            $(this).addClass('theme-black');                    
        },
        function(){
            $(this).removeClass('theme-black');                    
            $(this).addClass('theme-white');                    
        }
    );
   
});